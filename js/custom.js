$(document).ready(function () {

    $( ".select-ui" ).selectmenu();
    cloneMenu();

    //TOGGLE MENU
    $(document).on('click', '[data-menu]', function (e) {
        e.preventDefault();
        var btn = $(this),
            isActive = btn.hasClass('active'),
            div = document.createElement('div');

        div.className = 'menu-backdrop';

        btn[isActive ? 'removeClass' : 'addClass']('active');
        $('body')[isActive ? 'removeClass' : 'addClass']('open-menu');

        if(isActive){
            $('.menu-backdrop').remove();
        } else {
            $('body').append(div);
        }

        $(this).trigger('ml.menu.' + isActive ? 'hidden' : 'shown');

    });
     $(document).on('click touchstart', '.menu-backdrop', function (e) {
         e.preventDefault();
         $('[data-menu]').click();
     });



    //COLLAPSE MENU
    $(document).on('click', '[data-menu-dropdown] a', function (e) {
        e.preventDefault();
        var btn = $(this).parent(),
            isActive = btn.hasClass('active');

        btn[isActive ? 'removeClass' : 'addClass']('active');
        $(this).trigger('ml.collapse.' + isActive ? 'hidden' : 'shown');

    });


    // SCROLL BTN
    $(window).scroll(function () {
        var isVisible = $(window).scrollTop() > 100;
        $('.up')[isVisible ? 'addClass' : 'removeClass']('show');
    });

    $(document).on('click','.up', function (e) {
        $('html, body').animate({scrollTop: 0}, 400);
    });


    $('.sliding:eq(0)').addClass('disabled');

    $(document).on('click', '.sliding', function (e) {

        e.preventDefault();

        if($(this).hasClass('disabled')) return false;

        var table = $(this).siblings('table'),
            isNext =  $(this).hasClass('next'),
            count = table.find('tr:eq(0) th').length - 1,
            curIndex = table.find('th.active').index(),
            index = isNext ? curIndex + 1 : curIndex - 1,
            minIndex = 1;

        $('.sliding').removeClass('disabled');

        if(index >= count){
            $('.sliding:eq(1)').addClass('disabled');
        }

        if(index <= minIndex){
            $('.sliding:eq(0)').addClass('disabled');
        }

        index = index > count ? count : index;
        index = index < minIndex ? minIndex : index;

        table.find('tr').each(function (inx, tr) {
            $(tr).find('th').each(function (indx, td) {
                var isActive = indx == index;
                    $(td)[isActive ? 'addClass' : 'removeClass']('active');
            });
            $(tr).find('td').each(function (indx, td) {
                var isActive = indx == index;
                $(td)[isActive ? 'addClass' : 'removeClass']('active');
            });
        });





    });


});

function cloneMenu() {
    var html = $('header .top-menu .nav').html();
    $('.mobile-menu .top .nav').html(html);
}



// SORT TABLE

function sort(el) {
    var col_sort = el.innerHTML;
    var tr = el.parentNode;
    var thead = tr.parentNode;
    var table = thead.parentNode;
    var td, arrow, col_sort_num;

    for (var i=0; (td = tr.getElementsByTagName("th").item(i)); i++) {
        if (td.innerHTML == col_sort) {
            col_sort_num = i;

            arrow = $(td).find('.sort');
            arrow2 = $(td).find('.sort-big');
            if (td.prevsort == "y"){
                el.up = Number(!el.up);
            }else{
                td.prevsort = "y";
                el.up = 0;
            }

            arrow.find('i').removeClass('active');
            arrow2.find('i').removeClass('active');
            $(td).addClass('sorting');
            arrow.find('i:eq(' + el.up + ')').addClass('active');
            arrow2.find('i:eq(' + el.up + ')').addClass('active');

        }else{
            if (td.prevsort == "y"){
                td.prevsort = "n";
                $(td).removeClass('sorting');
                $(td).find('.sort-big i').removeClass('active');
                $(td).find('.sort i').removeClass('active');
            }
        }
    }

    var a = new Array();

    for(i=1; i < table.rows.length; i++) {
        a[i-1] = new Array();

        var td = table.rows[i].getElementsByTagName("td").item(col_sort_num),
            value = td.innerHTML;
        if($(td).hasClass('time')){
            var array = value.split(':');
            value = (+array[0]) * 60 * 60 + (+array[1]) * 60 ;
        }
        a[i-1][0]=value;
        a[i-1][1]=table.rows[i];

    }

    a.sort();
    if(el.up) a.reverse();

    for(i=0; i < a.length; i++)
        $(table).find('tbody').append(a[i][1]);
}



/*
// TOUCH EVENTS

document.addEventListener('touchstart', handleTouchStart, false);
document.addEventListener('touchmove', handleTouchMove, false);

var xDown = null;
var yDown = null;

function handleTouchStart(evt) {
    xDown = evt.touches[0].clientX;
    yDown = evt.touches[0].clientY;
}

function handleTouchMove(evt) {
    if ( ! xDown || ! yDown ) {
        return;
    }

    var xUp = evt.touches[0].clientX;
    var yUp = evt.touches[0].clientY;

    var xDiff = xDown - xUp;
    var yDiff = yDown - yUp;

    if ( Math.abs( xDiff ) > Math.abs( yDiff ) ) {
        if ( xDiff > 0 ) {
            $('body').removeClass('open-menu');
            $('[data-menu]').removeClass('active');
            $('.menu-backdrop').remove();
        } else {
            var div = document.createElement('div');
            div.className = 'menu-backdrop';
            $('body').append(div);
            $('body').addClass('open-menu');
            $('[data-menu]').addClass('active');
        }
    } else {
        if ( yDiff > 0 ) {
        } else {
        }
    }
    xDown = null;
    yDown = null;
}
    */