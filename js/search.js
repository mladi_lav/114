$(document).ready(function () {


    var availableTags = [
        "ActionScript",
        "AppleScript",
        "Asp",
        "BASIC",
        "C",
        "C++",
        "Clojure",
        "COBOL",
        "ColdFusion",
        "Erlang",
        "Fortran",
        "Groovy",
        "Haskell",
        "Java",
        "JavaScript",
        "Lisp",
        "Perl",
        "PHP",
        "Python",
        "Ruby",
        "Scala",
        "Scheme"
    ];
    $( ".city" ).autocomplete({
        source: availableTags
    });


    //REPLACE PLACE
    $(document).on('click', '[data-replace] a', function (e) {
        e.preventDefault();
        var btn = $(this),
            text = btn.text();

        btn.parents('ul').siblings('input').val(text).change();
    });


    $(document).on('click', '.btn-replace', function (e) {
        e.preventDefault();
        var btn = $(this),
            places = [],
            form = btn.parents('form'),
            inputs = form.find('input[type=text]');


        places.push($(inputs[0]).val());
        places.push($(inputs[1]).val());

        $(inputs[0]).val(places[1]);
        $(inputs[1]).val(places[0]);
    });


});