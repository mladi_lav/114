$(document).ready(function () {

    var mySwiper = new Swiper ('.swiper-container', {
        loop: false,
        slidesPerView: 5,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        breakpoints: {
            500: {
                slidesPerView: 1,
                spaceBetween: 0
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 0
            },
            992: {
                slidesPerView: 3,
                spaceBetween: 0
            },
            1080: {
                slidesPerView: 4,
                spaceBetween: 0
            }
        }
    });

});