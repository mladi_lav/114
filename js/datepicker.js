$(document).ready(function () {

    $.datepicker._updateDatepicker_original = $.datepicker._updateDatepicker;
    $.datepicker._updateDatepicker = function(inst) {
        $.datepicker._updateDatepicker_original(inst);
        var afterShow = this._get(inst, 'afterShow');
        if (afterShow)
            afterShow.apply((inst.input ? inst.input[0] : null));  // trigger custom callback
    };

    $.datepicker.setDefaults(
        $.extend(
            {'dateFormat':'dd-mm-yy'},
            $.datepicker.regional['ru']
        )
    );

    $( ".date" ).datepicker({
        numberOfMonths: 12,
        showButtonPanel: false,
        minDate: 0,
        buttonImageOnly: true,
        afterShow: function (e) {

            // create custom structure

            var datepicker = $('#ui-datepicker-div'),
                html = datepicker.html(),
                sidebar = document.createElement('div'),
                sidebarUl = document.createElement('ul'),
                calendar = document.createElement('div'),
                scrollPane = document.createElement('div'),
                backdrop,
                input = $(this),
                top = input.offset().top;


            input.addClass('active');
            header = document.createElement('ul');
            headerCalendar = document.createElement('table');


            $('body').append('<div class="datepicker-backdrop"></div>');

            backdrop = $('.datepicker-backdrop');

            $('.ui-datepicker-month').each(function (indx, el) {
                var li = document.createElement('li');
                if(indx < 2){
                    li.className = 'active';
                }
                li.innerHTML = $(el).text();
                sidebarUl.appendChild(li);
            });

            headerCalendar.innerHTML = $('.ui-datepicker-calendar thead').html();

            datepicker.html('');

            header.className = 'ui-calendar-head places';
            header.innerHTML = $('.places-date').html();
            sidebar.className = 'ui-calendar-sidebar';
            sidebar.appendChild(sidebarUl);
            calendar.className = 'ui-calendar-list';
            scrollPane.className = 'scroll-pane';
            headerCalendar.className = 'ui-calendar-weeks';
            scrollPane.innerHTML = html;

            calendar.appendChild(scrollPane);

            datepicker.append(header);
            datepicker.append(sidebar);
            datepicker.append(calendar);
            datepicker.append(headerCalendar);




            // click on places

            $(document).on('click', '.ui-calendar-head a', function (e) {
                e.preventDefault();
                var btn = $(this),
                    text = btn.text();

                input.val(text);
                input.change();
                $( ".date" ).datepicker( "hide" );

                backdrop = $('.datepicker-backdrop');
                backdrop.remove();
            });

            backdrop.on('click', function () {

                $( ".date" ).datepicker( "hide" );
                input.removeClass('active');

                backdrop = $('.datepicker-backdrop');
                backdrop.remove();
            });


            // select day

            $('[data-handler="selectDay"]').click(function () {

                var month = parseInt($(this).data('month')) + 1,
                    day = $(this).text();

                if(month < 10){
                    month = '0' + month;
                }

                if(day < 10){
                    day = '0' + day;
                }

                input.val(day + '.' + month + '.' + $(this).data('year'));
                input.change();
                input.removeClass('active');
                $( ".date" ).datepicker( "hide" );
                
                backdrop = $('.datepicker-backdrop');
                backdrop.remove();

            });



            // scoll

            var pane = $('.scroll-pane').jScrollPane();
            var api = pane.data('jsp');

            pane.bind(
                'jsp-will-scroll-y',
                function(event, destY)
                {
                    var index = Math.round(destY/32) ;
                    activeMonth(index);
                }
            );

            $(document).on('click', '.ui-calendar-sidebar li', function (e) {

                var index = $(this).index(),
                    height = $('.jspPane').height()/$('.ui-calendar-sidebar li').length;

                api.scrollToY(height * index);
                activeMonth(index);

            });



        }
    });



    $(document).on('click', '.calendar-icon, .mobile-calendar', function (e) {
        e.preventDefault();

        $( ".date" ).datepicker( "show" );
    });

    $('input[readonly]').on('focus', function(ev) {
        $(this).trigger('blur');
        var input = $(this),
            top = input.offset().top,
            datepicker = $('#ui-datepicker-div');

        datepicker.css({ top: top + 50 + 'px'});
    });


});

function activeMonth(index) {
    $('.ui-calendar-sidebar li').removeClass('active');

    $('.ui-calendar-sidebar li:eq(' + index + ')').addClass('active');
    $('.ui-calendar-sidebar li:eq(' + (index+1) + ')').addClass('active');

}