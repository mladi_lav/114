
$(document).ready(function () {

    ResizeLocality();

    $(window).resize(function () {
        ResizeLocality();
    });
});

function ResizeLocality() {

    if($('.table-locality').length > 0){

        var elements = [];

        $('.table-locality tr').each(function (inx, tr) {

            $(tr).find('td:eq(1)').each(function (inx, td) {

                $(td).find('p').each(function (inx, p) {
                    $(p).attr('style','');
                    elements.push($(p));
                });
            })
        });

        var i = 0;

        $('.table-locality tr').each(function (inx, tr) {

            $(tr).find('td:eq(2)').each(function (inx, td) {

                $(td).find('p').each(function (inx, p) {

                    $(p).attr('style','');
                    var height = $(p).height();
                    if(height >  elements[i].height()){
                        elements[i].height(height);
                    } else {
                        $(p).height(elements[i].height());
                    }
                    i++;
                });
            })
        });
    }

}
